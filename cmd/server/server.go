package main

import (
	"flag"
	"log"

	"gitlab.com/go-game-engine/engin"
)

var (
	listen = flag.String("listen", ":8000", "listen address")
)

func main() {
	flag.Parse()
	log.Printf("server: Starting on %q...", *listen)

	// Create TLS Server Listener
	tlsListener := engin.StartTLSListenServer(*listen)

	// Create the gRPC server
	grpcServer := engin.CreateGRPCServer()

	// Create Game
	myGame := engin.NewGame()
	myGame.Start()

	// Create Server Object to match gRPC interfaces and register them
	engin.RegisterGameServer(grpcServer, myGame)

	// Actually start the gRPC server
	engin.StartGRPCServer(tlsListener)

}
