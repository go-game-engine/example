package main

import (
	"io/fs"
	"log"
	"os"

	"gitlab.com/go-game-engine/engin"
)

func WriteLogsToFile(filepath string) {
	file, err := os.OpenFile(filepath, 7777, fs.FileMode(os.O_RDWR|os.O_TRUNC))
	if err != nil {
		log.Panicln(err)
	}
	log.SetOutput(file)
}

func main() {
	WriteLogsToFile("/home/joseph/Desktop/gitlab.com/go-game-engine/engin/assets/logs/client.log")

	// Create Network Reader
	gRPCClient := engin.StartWriterServer()

	// Create Game
	myGame := engin.NewGame()
	myGame.Start()

	// Create UI
	myView := engin.NewView(myGame)

	// Create Game Client
	client := engin.NewGameClient(myGame, myView)

	// Conenct Client
	client.Connect(gRPCClient, "Joseph", "test")

	// Start Client and UI
	client.Start()
	myView.Start()

	err := <-myView.Done
	if err != nil {
		log.Fatal(err)
	}
}
