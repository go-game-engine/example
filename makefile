certs:
	@echo "make server cert"
	@openssl req -new -nodes -x509 -out assets/certs/server/server.pem -keyout assets/certs/server/server.key -days 3650 -subj "/C=DE/ST=NRW/L=Earth/O=Random Company/OU=IT/CN=www.random.com/emailAddress=$1"

	@echo "make client cert"
	@openssl req -new -nodes -x509 -out assets/certs/client/client.pem -keyout assets/certs/client/client.key -days 3650 -subj "/C=DE/ST=NRW/L=Earth/O=Random Company/OU=IT/CN=www.random.com/emailAddress=$1"

proto:
	@protoc --go_out=plugins=grpc:internal assets/proto/framework.proto

